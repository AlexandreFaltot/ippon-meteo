//
//  NetworkConfiguration.swift
//  NetworkKit
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation
import Swinject

public let networkContainer = Container()

public protocol OpenWeatherConfigurationProtocol {
    var openWeatherApiKey: String { get set }
    var openWeatherUrl: URL { get set }
}
