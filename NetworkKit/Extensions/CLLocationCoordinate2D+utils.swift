//
//  CLLocationCoordinate2D+utils.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import MapKit


extension CLLocationCoordinate2D: Decodable {
    enum CodingKeys: String, CodingKey {
        case lat, lon
        case Lat, Lon
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        // Depending on the endpoint, the API has different keys for the lat / lon elements
        let latitude: Double = try (try? container.decode(.lat)) ?? container.decode(.Lat)
        let longitude: Double = try (try? container.decode(.lon)) ?? container.decode(.Lon)

        self.init(latitude: latitude,
                  longitude: longitude)
    }
}
