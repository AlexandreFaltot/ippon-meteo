//
//  DateFormatter+openWeatherApi.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation
import CommonKit

extension DateFormatter {
    /// The date formatter for the open weather api calls
    static var openWeatherApi: DateFormatter {
        let formatter: DateFormatter = .default
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }
}
