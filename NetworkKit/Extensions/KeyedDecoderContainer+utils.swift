//
//  KeyedDecoderContainer+utils.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

extension KeyedDecodingContainer {
    ///
    /// Decodes the element for the given key by using the provided transformer
    ///
    /// - parameters:
    ///     * key: The key of the element to decode
    ///     * transformer: The transformer that will decode the element
    ///
    /// - returns: The decoded data by the transformer
    ///
    func decode<Transformer: DecodingContainerTransformer>(_ key: KeyedDecodingContainer.Key, using transformer: Transformer) throws -> Transformer.Output where Transformer.Input : Decodable {
        let decoded: Transformer.Input = try self.decode(key)
        return try transformer.transform(decoded)
    }

    ///
    /// Decodes the element for the given key by using its inferred type
    ///
    /// - parameter key: The element key
    /// - returns: The decoded element
    ///
    func decode<T>(_ key: KeyedDecodingContainer.Key) throws -> T where T : Decodable {
        return try self.decode(T.self, forKey: key)
    }
}
