//
//  NetworkError.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public enum NetworkError: Error {
    case malformedUrlRequest
    case decodingResponseError(error: Error)
    case requestError(urlResponse: URLResponse?, error: Error?)
}
