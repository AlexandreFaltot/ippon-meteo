//
//  DecodingContainerTransformer.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

protocol DecodingContainerTransformer {
    associatedtype Input
    associatedtype Output

    func transform(_ decoded: Input) throws -> Output
}

