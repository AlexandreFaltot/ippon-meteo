//
//  JSONDateTransformer.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

class JSONDateTransformer: DecodingContainerTransformer {
    typealias Input = String
    typealias Output = Date

    let formatter: DateFormatter

    init(formatter: DateFormatter = .openWeatherApi) {
        self.formatter = formatter
    }

    func transform(_ decoded: String) throws -> Date {
        guard let date = formatter.date(from: decoded) else {
            throw JSONDateError(inputDate: decoded)
        }

        return date
    }

    struct JSONDateError: Error {
        var inputDate: String
    }
}

