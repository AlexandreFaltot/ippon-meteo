//
//  WeatherForecast.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public struct WeatherForecast: Decodable {
    public var dataPoint: Int
    public var main: Main
    public var weather: [Weather]
    public var clouds: Clouds
    public var wind: Wind
    public var visibility: Int
    public var probabilityOfPrecipitation: Double
    public var sys: System
    public var date: Date

    enum CodingKeys: String, CodingKey {
        case dataPoint = "dt"
        case date = "dt_txt"
        case probabilityOfPrecipitation = "pop"
        case main ,weather ,clouds ,wind ,visibility ,rain ,sys
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.dataPoint = try container.decode(.dataPoint)
        self.main = try container.decode(.main)
        self.weather = try container.decode(.weather)
        self.clouds = try container.decode(.clouds)
        self.wind = try container.decode(.wind)
        self.visibility = try container.decode(.visibility)
        self.probabilityOfPrecipitation = try container.decode(.probabilityOfPrecipitation)
        self.sys = try container.decode(.sys)
        self.date = try container.decode(.date, using: JSONDateTransformer())
    }
}
