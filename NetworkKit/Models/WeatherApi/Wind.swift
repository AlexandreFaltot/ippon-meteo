//
//  Wind.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public struct Wind: Decodable {
    public var speed: Double
    public var degrees: Double

    enum CodingKeys: String, CodingKey {
        case speed
        case degrees = "deg"
    }
}
