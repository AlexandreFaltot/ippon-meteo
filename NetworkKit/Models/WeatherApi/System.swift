//
//  System.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public struct System: Decodable {
    public var type: Int?
    public var id: Int?
    public var message: Double?
    public var country: String?
    public var sunrise: Int?
    public var sunset: Int?
    public var pod: String?
}
