//
//  City.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation
import MapKit

public struct City: Decodable {
    public var sunset: Int?
    public var country: String?
    public var id: Int
    public var coord: CLLocationCoordinate2D?
    public var population: Int?
    public var timezone: Int?
    public var sunrise: Int?
    public var name: String

    public init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}
