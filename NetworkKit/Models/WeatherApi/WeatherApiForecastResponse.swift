//
//  WeatherApiForecastResponse.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public struct WeatherAPIForecastResponse: Decodable {
    public var list: [WeatherForecast]
    public var city: City
}
