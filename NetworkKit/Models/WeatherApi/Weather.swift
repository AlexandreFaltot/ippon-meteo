//
//  Weahter.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public struct Weather: Decodable {
    public var id: Int
    public var main: String
    public var description: String
    public var icon: WeatherIcon
}

