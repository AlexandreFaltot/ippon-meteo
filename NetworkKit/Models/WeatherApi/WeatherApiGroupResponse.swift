//
//  WeatherApiGroupResponse.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public struct WeatherAPIGroupResponse: Decodable {
    public var list: [WeatherAPIStation]
}
