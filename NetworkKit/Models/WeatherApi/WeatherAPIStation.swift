//
//  WeatherAPIResponse.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import MapKit

public struct WeatherAPIStation: Decodable {
    public var coordinates: CLLocationCoordinate2D
    public var weathers: [Weather]
    public var base: String?
    public var main: Main
    public var visibility: Int
    public var wind: Wind
    public var clouds: Clouds
    public var dataPoint: Int
    public var system: System?
    public var timezone: Int?
    public var id: Int
    public var name: String
    public var code: Int?

    enum CodingKeys: String, CodingKey {
        case coordinates = "coord"
        case weathers = "weather"
        case system = "sys"
        case dataPoint = "dt"
        case base, main, visibility, wind, clouds, timezone, id, name, code
    }
}
