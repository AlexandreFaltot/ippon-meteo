//
//  WeatherIcon.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public enum WeatherIcon: String, Decodable {
    // Day icons
    case clearDay = "01d"
    case fewCloudsDay = "02d"
    case scatteredCloudsDay = "03d"
    case overcastCloudsDay = "04d"
    case rainDay = "10d"
    case mistDay = "50d"
    case drizzleDay = "09d"

    // Night icons
    case clearNight = "01n"
    case fewCloudsNight = "02n"
    case scatteredCloudsNight = "03n"
    case overcastCloudsNight = "04n"
    case rainNight = "10n"
    case mistNight = "50n"
    case drizzleNight = "09n"
}
