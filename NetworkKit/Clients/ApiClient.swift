//
//  NetworkWorker.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 28/04/2021.
//

import Foundation
import Combine
import CommonKit

public class ApiClient<T: Target> {

    public init() {}

    ///
    /// Creates and sends an HTTP Request
    ///
    /// - parameters:
    ///     * expectedType: The type that will be used to decode
    ///                     the response data in case of success
    ///     * target: The target that defines the URL request
    ///
    /// - returns: A publisher that handle the request
    ///
    public func request<Response: Decodable>(_ expectedType: Response.Type, target: T) -> AnyPublisher<Response, Error> {
        guard let request = target.asURLRequest() else {
            Logger.logError("Error with target: \(target)")
            return Fail<Response, Error>(error: TargetURLError())
                .eraseToAnyPublisher()
        }

        // Launch the request
        Logger.logDebug(request.logRepresentation)
        return URLSession.shared.dataTaskPublisher(for: request)
            .tryMap { data, response -> Data in
                // Map and validate the response
                guard let httpResponse = response as? HTTPURLResponse,
                      (200...299).contains(httpResponse.statusCode) else {
                    throw URLError(.badServerResponse)
                }

                Logger.logDebug(httpResponse.logRepresentation(request: request, data: data))
                return data
            }
            .decode(type: Response.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .mapError {
                Logger.logError("Error with request: \($0.localizedDescription)")
                return $0
            }
            .eraseToAnyPublisher()
    }
}

struct TargetURLError: Error {}
