//
//  WeatherTarget.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import Foundation

public enum ForecastTarget: Target {
    case futureDaysForecast(id: Int)
    case cities(ids: [Int])

    public var endpoint: String {
        switch self {
        case .futureDaysForecast: return "/data/2.5/forecast"
        case .cities: return "/data/2.5/group"
        }
    }

    public var queryParameters: [String: String]? {
        switch self {
        case .futureDaysForecast(let id): return ["id": id.description,
                                                  "units": "metric"]
        case .cities(let ids): return ["id": ids
                                        .map { String($0) }
                                        .joined(separator: ","),
                                       "units": "metric"]
        }
    }

    public var httpBody: Data? { nil }
}
