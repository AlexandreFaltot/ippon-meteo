//
//  Target.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import Foundation

public protocol Target {
    /// The base url for the target
    var baseUrl: URL? { get }
    /// The endpoint for the target
    var endpoint: String { get }
    /// The query paramaters for the target
    var queryParameters: [String: String]? { get }
    /// The http body to send for the target
    var httpBody: Data? { get }

    ///
    /// The method that builds the URL Request
    /// from the target
    ///
    func asURLRequest() -> URLRequest?
}

public extension Target {
    var baseUrl: URL? { networkContainer.resolve(OpenWeatherConfigurationProtocol.self)?.openWeatherUrl }
    var httpBody: Data? { nil }
    var queryParameters: [String: String]? { nil }

    func asURLRequest() -> URLRequest? {
        guard let baseUrl = baseUrl else {
            return nil
        }

        // Create url components
        var components = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true)
        components?.path = endpoint
        components?.queryItems = queryParameters?.map { URLQueryItem(name: $0.key, value: $0.value) }

        // Add the Open Weather API key
        if let apiKey = networkContainer.resolve(OpenWeatherConfigurationProtocol.self)?.openWeatherApiKey {
            components?.queryItems?.append(URLQueryItem(name: "appid", value: apiKey))
        }

        // Build the request with the body
        var request = components?.url.map { URLRequest(url: $0) }
        request?.httpBody = httpBody

        return request
    }
}

