//
//  Strings.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public class Strings {
    /// Wind
    public static var windTitle: String { "iw.weather.row.wind.title".localize() }
    /// Pre.
    public static var precipitationTitle: String { "iw.weather.row.precipitation.title".localize() }
    /// Clouds
    public static var cloudsTitle: String { "iw.weather.row.clouds.title".localize() }

    /// %d %@
    public static func unitIntFormat(_ value: Int, _ unit: Units) -> String {
        "iw.integer.unit".localize(value, unit.rawValue)
    }

    /// %.2f %@
    public static func unitDoubleFormat(_ value: Double, _ unit: Units) -> String {
        "iw.double.unit".localize(value, unit.rawValue)
    }
}

private extension String {
    ///
    /// Gives the localized version for the key defined by the string itself
    ///
    /// - parameter args: The arguments for the string format
    /// - returns: The localized version of the string
    ///
    func localize(_ args: CVarArg...) -> String {
        let bundle = Bundle(for: Strings.self)
        let format = bundle.localizedString(forKey: self, value: nil, table: "Localizable")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}
