//
//  Units.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public enum Units: String {
    case speed = "km/h"
    case temperature = "°C"
    case orientation = "°"
    case percent = "%"
}
