//
//  Logger.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 28/04/2021.
//

import Foundation

public class Logger {
    static public func logError(_ message: Any?, file: String = #file, line: Int = #line, function: String = #function) {
        #if DEBUG
        print("\(Date()) [\(file):\(line)] 🟥 In function \(function) - \(message ?? "nil")")
        #endif
    }

    static public func logDebug(_ message: Any?, file: String = #file, line: Int = #line, function: String = #function) {
        #if DEBUG
        print("\(Date()) [\(file):\(line)] 🟦 In function \(function) - \(message ?? "nil")")
        #endif
    }

    static public func logWarning(_ message: Any?, file: String = #file, line: Int = #line, function: String = #function) {
        #if DEBUG
        print("\(Date()) [\(file):\(line)] 🟨 In function \(function) - \(message ?? "nil")")
        #endif
    }

    static public func logInfo(_ message: Any?, file: String = #file, line: Int = #line, function: String = #function) {
        #if DEBUG
        print("\(Date()) [\(file):\(line)] ⬜ In function \(function) - \(message ?? "nil")")
        #endif
    }
}
