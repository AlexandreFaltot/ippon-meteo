//
//  URLRequest+logs.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public extension URLRequest {
    /// Log string for the request
    /// Displays the url, the method, the parameters, and the headers
    var logRepresentation: String {
        let urlRequestLogString = ["REQUESTING:", httpMethod, url?.absoluteString]
            .compactMap { $0 }
            .joined(separator: " ")

        let headersLogString = allHTTPHeaderFields?.logRepresentation.map { "HEADERS:\n\($0)" }
        let bodyLogString = httpBody?.logRepresentation.map { "PARAMETERS: \($0)" }
        return [urlRequestLogString,
                headersLogString,
                bodyLogString]
            .compactMap { $0 }
            .joined(separator: "\n")
    }
}

public extension HTTPURLResponse {
    /// Log string for the response
    /// Displays the url, the status code, the headers, and the response body
    func logRepresentation(request: URLRequest, data: Data?) -> String {
        let urlRequestLogString: String = [statusCode.description, request.httpMethod, url?.absoluteString]
            .compactMap { $0 }
            .joined(separator: " ")

        let headersLogString = (allHeaderFields as? [String: Any])?.logRepresentation.map { "HEADERS:\n\($0)" }
        let bodyLogString = data?.logRepresentation.map { "RESPONSE:\n\($0)" } ?? "No response available"
        return [urlRequestLogString,
                headersLogString,
                bodyLogString]
            .compactMap { $0 }
            .joined(separator: "\n")
    }
}

public extension Data {
    /// A log representation of the data, assuming the data is
    /// represented in a json format
    var logRepresentation: String? {
        guard !isEmpty else { return nil }

        do {
            let jsonResult = try JSONSerialization.jsonObject(with: self, options: [])
            let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: [.prettyPrinted])
            return NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
        } catch {
            return String(data: self, encoding: .utf8)
        }
    }
}

public extension Dictionary where Key == String, Value: Any {
    /// Log string for the dictionnay, displayed as a json if can be
    var logRepresentation: String? {
        guard !isEmpty else { return nil }

        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
            return NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as String?
        } catch {
            return String(describing: self)
        }
    }
}
