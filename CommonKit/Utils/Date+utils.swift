//
//  Date+utils.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 01/05/2021.
//

import Foundation

public extension Date {
    ///
    /// Give the date in a form of a string, defined by
    /// the provided date formatter
    ///
    /// - parameter formatter: The `DateFormatter` that will be used to
    ///                        give the string
    /// - returns: The date in a form of a string
    ///
    func string(from formatter: DateFormatter) -> String {
        formatter.string(from: self)
    }

    ///
    /// A subscript that gives the asked date component
    ///
    /// - parameter component: The component to retrieve
    /// - returns: The component value given by the current calendar
    ///
    subscript (_ component: Calendar.Component) -> Int {
        Calendar.current.component(component, from: self)
    }
}


public extension DateFormatter {
    /// The default date formatter to use
    static var `default`: DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter
    }

    /// The formatter that display a date for human users
    static var humanDate: DateFormatter {
        let formatter: DateFormatter = .default
        formatter.dateFormat = "EEE dd"
        return formatter
    }
}
