//
//  Number+strUtils.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

public extension Double {
    ///
    /// Gives a string that displays the value with the
    /// provided unit
    ///
    /// - parameter unit: The unit used for the string format
    /// - returns: A string version of the value with the unit
    ///
    func format(with unit: Units) -> String {
        Strings.unitDoubleFormat(self, unit)
    }
}

public extension Int {
    ///
    /// Gives a string that displays the value with the
    /// provided unit
    ///
    /// - parameter unit: The unit used for the string format
    /// - returns: A string version of the value with the unit
    ///
    func format(with unit: Units) -> String {
        Strings.unitIntFormat(self, unit)
    }
}
