//
//  StringsTests.swift
//  StringsTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
@testable import CommonKit

class StringsTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("Strings") {
            context("Localizing strings") {
                it("should localize strings nicely") {
                    expect(Strings.windTitle).to(equal("Wind"))
                    expect(Strings.precipitationTitle).to(equal("Pre."))
                    expect(Strings.cloudsTitle).to(equal("Clouds"))
                    expect(Strings.unitIntFormat(100, .speed)).to(equal("100 km/h"))
                    expect(Strings.unitIntFormat(100, .orientation)).to(equal("100 °"))
                    expect(Strings.unitDoubleFormat(31.342534, .percent)).to(equal("31.34 %"))
                    expect(Strings.unitDoubleFormat(31.342534, .temperature)).to(equal("31.34 °C"))
                }
            }
        }
    }
}
