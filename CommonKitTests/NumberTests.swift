//
//  NumberTests.swift
//  CommonKitTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
@testable import CommonKit

class NumberTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("Number") {
            context("formatting to string") {
                it("should format nicely") {
                    let int: Int = 1
                    let double: Double = 0.34
                    expect(int.format(with: .percent)).to(equal(Strings.unitIntFormat(int, .percent)))
                    expect(double.format(with: .orientation)).to(equal(Strings.unitDoubleFormat(double, .orientation)))
                }
            }
        }
    }
}
