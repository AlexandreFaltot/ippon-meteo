//
//  LogTests.swift
//  CommonKitTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
@testable import CommonKit

class LogTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("Logs") {
            context("URLRequest log") {
                it("should give a log string") {
                    expect(URLRequest(url: URL(string: "https://api.weather.org")!).logRepresentation).toNot(beEmpty())
                }
            }

            context("JSON Data log") {
                it("should give a log string") {
                    let data = #"{"hello":"world"}"#.data(using: .utf8)
                    expect(data?.logRepresentation).toNot(beEmpty())
                }
            }

            context("Dictionnary log") {
                it("should give a log string") {
                    let dict: [String: Any] = ["Key_1": "Hello",
                                               "Key_2": 123]
                    expect(dict.logRepresentation).toNot(beEmpty())
                }
            }
        }
    }
}
