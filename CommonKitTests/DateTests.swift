//
//  DateTests.swift
//  CommonKitTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
@testable import CommonKit

class DateTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("Date") {
            let formatter = DateFormatter.default
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date: Date! = formatter.date(from: "2020-01-01 12:00:00")

            context("formatting to string") {
                it("should format nicely with the formatter") {
                    expect(date.string(from: .default)).to(equal(""))
                    expect(date.string(from: .humanDate)).to(equal("Wed 01"))
                }
            }

            context("subscripting") {
                it("should give the right information") {
                    expect(date[.day]).to(equal(1))
                    expect(date[.year]).to(equal(2020))
                    expect(date[.month]).to(equal(01))
                }
            }
        }
    }
}
