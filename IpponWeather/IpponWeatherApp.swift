//
//  IpponWeatherApp.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 28/04/2021.
//

import SwiftUI
import Swinject
import NetworkKit

public let appContainer = Container()

@main
struct IpponWeatherApp: App {
    init() {
        /// Register objects for dependency injection
        networkContainer
            .register(OpenWeatherConfigurationProtocol.self) { _ in try! IpponWeatherConfiguration.decodedFromPlist() }
            .inObjectScope(.container)
        appContainer
            .register(IWStationDetailViewModel.self) { IWStationDetailViewModel(service: $0.resolve(WeatherService.self) ?? WeatherService()) }
            .inObjectScope(.container)
        appContainer
            .register(IWMapViewModel.self) { IWMapViewModel(service: $0.resolve(WeatherService.self) ?? WeatherService()) }
            .inObjectScope(.container)
        appContainer
            .register(WeatherService.self) { _ in WeatherService() }
            .inObjectScope(.container)
    }

    var body: some Scene {
        WindowGroup {
            IWViewControllersProvider.home()
        }
    }
}
