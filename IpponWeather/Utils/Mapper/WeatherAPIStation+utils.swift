//
//  WeatherAPIStation+utils.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import InterfaceKit
import NetworkKit

extension WeatherAPIStation: Identifiable {
    /// Gives the associated weather forecast preview model for the station
    var iwWeatherForecastPreviewViewModel: IWWeatherForecastPreviewViewModel {
        IWWeatherForecastPreviewViewModel(image: weathers.first?.icon.image,
                                          temperature: main.temperature)
    }
}

