//
//  WeatherIcon+image.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import NetworkKit
import SwiftUI

extension WeatherIcon {
    /// The image that should be displayed for the icon
    var image: Image {
        switch self {
        case .clearDay, .clearNight: return Image("sunny")
        case .fewCloudsDay, .fewCloudsNight: return Image("sunnyAndCloudy")
        case .scatteredCloudsDay, .scatteredCloudsNight: return Image("cloudy")
        case .overcastCloudsDay, .overcastCloudsNight: return Image("cloudyAndWindy")
        case .rainDay, .rainNight: return Image("rainy")
        case .mistDay, .mistNight: return Image("cloudyAndWindy")
        case .drizzleDay, .drizzleNight: return Image("thunder")
        }
    }
}
