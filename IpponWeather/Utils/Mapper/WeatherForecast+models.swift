//
//  WeatherForecast+models.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation
import CommonKit
import NetworkKit
import InterfaceKit

extension WeatherForecast {

    /// Gives the associated weather row model for the forecast
    var iwWeatherRow: IWWeatherRowModel? {
        guard let weather = weather.first else { return nil }

        return IWWeatherRowModel(dateTime: date,
                                 weatherIcon: weather.icon.image,
                                 temperature: main.temperature,
                                 wind: (wind.speed, wind.degrees),
                                 precipitation: probabilityOfPrecipitation,
                                 clouds: clouds.all ?? 0)
    }

    /// Gives the associated weather preview model for the forecast
    var iwWeatherForecastPreview: IWWeatherForecastPreviewViewModel? {
        guard let weather = weather.first else { return nil }
        return IWWeatherForecastPreviewViewModel(title: date.string(from: .humanDate),
                                                 image: weather.icon.image,
                                                 temperature: main.temperature)
    }
}
