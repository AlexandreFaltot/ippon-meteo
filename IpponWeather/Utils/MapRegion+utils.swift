//
//  MapRegion+utils.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import MapKit

extension MKCoordinateRegion {
    /// Gives the region of France for the Map
    static var metropolitanFrance: MKCoordinateRegion {
        MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 47.260833,
                                                          longitude: 2.418889),
                           latitudinalMeters: 1_250_000,
                           longitudinalMeters: 1_250_000)
    }
}
