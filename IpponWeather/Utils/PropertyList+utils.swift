//
//  PropertyList+utils.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation

protocol PropertyListDecodable: Decodable {}

extension PropertyListDecodable {
    ///
    /// Decodes a `.plist` file based on the current type
    ///
    static func decodedFromPlist() throws -> Self {
        guard let file = Bundle.main.url(forResource: String(describing: self), withExtension: "plist") else {
            throw IWError.noUrlForFile
        }

        let data = try Data(contentsOf: file)
        return try PropertyListDecoder().decode(Self.self, from: data)
    }
}

enum IWError: Error {
    case noUrlForFile
}
