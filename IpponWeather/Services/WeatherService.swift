//
//  WeatherService.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import Foundation
import Combine
import NetworkKit

class WeatherService {
    var apiClient = ApiClient<ForecastTarget>()

    ///
    /// Fetch the current forecast of the provided cities from the API
    ///
    /// - parameter cities: The list of cities for the API call
    /// - returns: A publisher that will handle the API result
    ///
    func getCurrentForecast(for cities: [City]) -> AnyPublisher<[WeatherAPIStation], Error> {
        let citiesIds = cities.compactMap { $0.id }
        return apiClient.request(WeatherAPIGroupResponse.self,
                                 target: .cities(ids: citiesIds))
            .map { $0.list }
            .eraseToAnyPublisher()
    }

    ///
    /// Fetch the future forecasts for the city with the given id
    ///
    /// - parameter id: The id of the city for the API call
    /// - returns: A publisher that will handle the API result
    ///
    func getFutureDaysForecast(for id: Int) -> AnyPublisher<WeatherAPIForecastResponse, Error> {
        return apiClient.request(WeatherAPIForecastResponse.self,
                                 target: .futureDaysForecast(id: id))
            .eraseToAnyPublisher()
    }
}
