//
//  File.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import Foundation
import CommonKit
import NetworkKit

class CityProvider {
    /// Gives the list of cities stored in the associated json file
    static var cities: [City] {
        let path = Bundle.main.path(forResource: "Cities", ofType: "json") ?? ""

        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            return try JSONDecoder().decode([City].self, from: data)
        } catch {
            Logger.logError("There was a problem decoding desired file City.json at path \(path) : \(error.localizedDescription)")
            return []
        }
    }
}
