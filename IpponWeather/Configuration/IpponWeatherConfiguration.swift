//
//  Config.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation
import NetworkKit

struct IpponWeatherConfiguration: PropertyListDecodable, OpenWeatherConfigurationProtocol {
    /// The API Key for the Open Weather API
    var openWeatherApiKey: String

    /// The base url of the Open Weather API
    var openWeatherUrl: URL
}
