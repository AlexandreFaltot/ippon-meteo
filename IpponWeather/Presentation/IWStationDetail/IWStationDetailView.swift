//
//  IWStationDetail.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import SwiftUI
import Combine
import InterfaceKit
import NetworkKit

struct IWStationDetailView: IWViewController {
    typealias ViewModel = IWStationDetailViewModel

    @ObservedObject var viewModel: IWStationDetailViewModel
    @Environment(\.presentationMode) var presentationMode

    init(viewModel: IWStationDetailViewModel) {
        self.viewModel = viewModel
        viewModel.fetchWeatherData()
    }

    var body: some View {
        switch viewModel.state {
        case .success(let station): return AnyView(successView(for: station))
        case .loading: return AnyView(loadingView)
        case .error: return AnyView(errorView)
        }
    }

    ///
    /// The method that gives the view in case of a
    /// successful request from the API
    ///
    /// - parameter station: The station containing the informations
    ///                      to display
    /// - returns: The view to diplay
    ///
    func successView(for station: WeatherAPIForecastResponse) -> some View {
        VStack(spacing: Style.Spacing.standard) {
            Text(station.city.name)
                .font(Style.TextFont.title)
            HStack {
                ForEach(viewModel.forecastsPreviews, id: \.date) { item in
                    item.iwWeatherForecastPreview
                        .map { IWWeatherForecastPreviewView(model: $0) }
                        .onTapGesture {
                            viewModel.focusOn(item.date)
                        }
                }
            }
            List(viewModel.displayedList, id: \.dataPoint) {
                $0.iwWeatherRow.map { IWWeatherRow(model: $0) }
            }
        }
        .padding(Style.Padding.standard)
    }

    /// The view to display when loading
    var loadingView: some View {
        ProgressView()
    }

    /// The view to display in case of error
    var errorView: some View {
        Text("Error fetching data")
    }
}

