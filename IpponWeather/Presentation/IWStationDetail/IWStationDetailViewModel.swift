//
//  IWStationDetailViewModel.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 01/05/2021.
//

import Combine
import Foundation
import InterfaceKit
import NetworkKit

class IWStationDetailViewModel: IWViewModel {
    /// The state of the viewModel
    private(set) var state: IWViewState<WeatherAPIForecastResponse> = .loading {
        willSet {
            objectWillChange.send()
        }
    }

    /// The list of forecast that should be displayed
    private(set) var displayedList: [WeatherForecast] = [] {
        willSet {
            objectWillChange.send()
        }
    }

    ///
    /// The list of forecast that should be used
    /// for the next days previews.
    ///
    var forecastsPreviews: [WeatherForecast] {
        switch state {
        case .success(let station):
            let previewForecastHour = station.list.first.map { $0.date[.hour] }
            return station.list
                .filter { $0.date[.hour] == previewForecastHour }
        default:
            return []
        }
    }

    /// The service used to get the data
    private var service: WeatherService
    /// The set of cancellable to handle
    private var cancellableSet = Set<AnyCancellable>()
    /// The city which will be used for the service call
    var cityToFetch: City?

    init(service: WeatherService) {
        self.service = service
        super.init()
    }

    ///
    /// Make the displayed list of weather which day are in
    /// the same day as the provided date
    ///
    /// - parameter date; The date which day will be focused on
    ///
    func focusOn(_ date: Date) {
        switch state {
        case .success(let value):

            // Filter the list basing on the date
            displayedList = value.list
                .filter { Calendar.current.isDate($0.date, inSameDayAs: date) }
                .sorted(by: { $0.date < $1.date })
        default:
            displayedList = []
        }
    }

    ///
    /// Fetch the data from the weather service, based on the
    /// city provided in the viewModel.
    ///
    /// Updates the state to loading while fetching the data
    ///
    func fetchWeatherData() {
        guard let cityId = cityToFetch?.id else {
            return self.state = .error(error: NoCityProvidedError())
        }

        self.state = .loading
        service.getFutureDaysForecast(for: cityId)
            .sink(receiveCompletion: { _ in },
                  receiveValue: {
                    self.state = .success(value: $0)
                    self.focusOn(Date())
                  })
            .store(in: &cancellableSet)
    }
}

struct NoCityProvidedError: Error {}
