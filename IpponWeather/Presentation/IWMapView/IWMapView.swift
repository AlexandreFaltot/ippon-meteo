//
//  IWMapView.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import SwiftUI
import MapKit
import InterfaceKit
import NetworkKit

struct IWMapView: IWViewController {
    typealias ViewModel = IWMapViewModel

    @ObservedObject var viewModel: IWMapViewModel
    @State private(set) var region: MKCoordinateRegion = .metropolitanFrance
    @State private(set) var selectedStation: WeatherAPIStation?

    var body: some View {
        Map(coordinateRegion: $region,
            interactionModes: .all,
            annotationItems: viewModel.annotations,
            annotationContent: { item in
                MapAnnotation(coordinate: item.coordinates, anchorPoint: CGPoint(x: 0.5, y: 0.5)) {
                    IWWeatherForecastPreviewView(model: item.iwWeatherForecastPreviewViewModel)
                        .onTapGesture(count: 1) {
                            selectedStation = item
                        }
                }
            })
            .edgesIgnoringSafeArea(.all)
            .onAppear {
                viewModel.fetchMajorCitiesWeather()
            }
            .sheet(item: $selectedStation) {
                IWViewControllersProvider.stationDetailsViewController(selectedItem: $0)
            }

    }
}
