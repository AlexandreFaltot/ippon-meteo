//
//  IWMapViewModel.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import SwiftUI
import MapKit
import Combine
import InterfaceKit
import NetworkKit

class IWMapViewModel: IWViewModel {
    /// The annotations that should be displayed on the map
    private(set) var annotations: [WeatherAPIStation] = [] {
        didSet {
            objectWillChange.send()
        }
    }

    /// The service used to get the data
    private var service: WeatherService
    /// The set of cancellable to handle
    private var cancellableSet: Set<AnyCancellable> = []

    init(service: WeatherService) {
        self.service = service
    }

    ///
    /// Gets the annotations from the viewModel service.
    /// In case of success, updates the `annotations` value
    ///
    func fetchMajorCitiesWeather() {
        service.getCurrentForecast(for: CityProvider.cities)
            .sink(receiveCompletion: { _ in },
                  receiveValue: { self.annotations = $0 })
            .store(in: &cancellableSet)

    }
}
