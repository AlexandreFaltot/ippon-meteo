//
//  IWViewControllersProvider.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 01/05/2021.
//

import Foundation
import NetworkKit

class IWViewControllersProvider {

    ///
    /// Gives the home page of the app
    ///
    /// - returns: The home page (i.e. the map view)
    ///
    static func home() -> IWMapView? {
        appContainer
            .resolve(IWMapViewModel.self)
            .map { IWMapView(viewModel: $0) }
    }

    ///
    /// Gives the station detail page
    ///
    /// - parameter selectedItem: The data to provide to the viewModel
    /// - returns: The station detail page
    ///
    static func stationDetailsViewController(selectedItem: WeatherAPIStation?) -> IWStationDetailView? {
        guard let viewModel = appContainer.resolve(IWStationDetailViewModel.self) else { return nil }

        viewModel.cityToFetch = selectedItem.map { City(id: $0.id, name: $0.name) }
        return IWStationDetailView(viewModel: viewModel)
    }
}
