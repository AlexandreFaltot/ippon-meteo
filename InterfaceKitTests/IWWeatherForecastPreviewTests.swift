//
//  IWWeatherForecastPreviewTests.swift
//  InterfaceKitTests
//
//  Created by Alexandre FALTOT on 03/05/2021.
//

import Quick
import Nimble
import ViewInspector
import CommonKit
import SwiftUI
@testable import InterfaceKit

class IWWeatherForecastPreviewViewTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("IWWeatherRow") {
            var view: IWWeatherForecastPreviewView!

            context("Initializing") {
                it("should inflate nicely with model") {
                    view = IWWeatherForecastPreviewView(model: IWWeatherForecastPreviewView.Model(image: nil, temperature: 10))
                    expect(view.model.temperature).to(equal(10))
                    expect(view.model.image).to(beNil())
                    expect(view.model.title).to(beNil())
                    expect(try? view.body.inspect().findAll(ViewType.Text.self).compactMap { try? $0.string() }).to(equal([view.model.displayedTemperature]))
                    expect(try? view.body.inspect().findAll(ViewType.Image.self).count).to(equal(0))

                    view = IWWeatherForecastPreviewView(model: IWWeatherForecastPreviewView.Model(title: "Title", image: nil, temperature: 10))
                    expect(view.model.temperature).to(equal(10))
                    expect(view.model.image).to(beNil())
                    expect(view.model.title).to(equal("Title"))
                    expect(try? view.body.inspect().findAll(ViewType.Text.self).compactMap { try? $0.string() }).to(equal(["Title", view.model.displayedTemperature]))
                    expect(try? view.body.inspect().findAll(ViewType.Image.self).count).to(equal(0))

                    view = IWWeatherForecastPreviewView(model: IWWeatherForecastPreviewView.Model(image: Image("sunny"), temperature: 10))
                    expect(view.model.temperature).to(equal(10))
                    expect(view.model.image).toNot(beNil())
                    expect(view.model.title).to(beNil())
                    expect(try? view.body.inspect().findAll(ViewType.Text.self).compactMap { try? $0.string() }).to(equal([try? view.model.image?.name(),
                                                                                                                           view.model.displayedTemperature]))
                    expect(try? view.body.inspect().findAll(ViewType.Image.self).count).to(equal(1))


                }
            }

            context("Model") {
                it("should give informations properly") {
                    let model = IWWeatherForecastPreviewView.Model(image: nil, temperature: 10)
                    expect(model.displayedTemperature).to(equal(Int(model.temperature).format(with: .temperature)))
                }
            }
        }
    }
}
