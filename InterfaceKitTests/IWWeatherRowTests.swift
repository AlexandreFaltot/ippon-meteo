//
//  InterfaceKitTests.swift
//  InterfaceKitTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
import ViewInspector
import CommonKit
@testable import InterfaceKit

class IWWeatherRowTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("IWWeatherRow") {
            let model = IWWeatherRow.Model(dateTime: Date(),
                                           temperature: 10,
                                           wind: (20, 30),
                                           precipitation: 40,
                                           clouds: 50)
            let view = IWWeatherRow(model: model)

            context("Initializing") {
                it("should inflate nicely with model") {
                    expect(view.model.dateTime).to(equal(model.dateTime))
                    expect(view.model.temperature).to(equal(model.temperature))
                    expect(view.model.wind.speed).to(equal(model.wind.speed))
                    expect(view.model.wind.degrees).to(equal(model.wind.degrees))
                    expect(view.model.precipitation).to(equal(model.precipitation))
                    expect(view.model.clouds).to(equal(model.clouds))

                    expect(try? view.body.inspect().findAll(ViewType.Text.self).compactMap { try? $0.string() }).to(equal([view.model.displayedTime,
                                                                                                                           view.model.displayedTemperature,
                                                                                                                           Strings.windTitle,
                                                                                                                           view.model.displayedWindSpeed,
                                                                                                                           view.model.displayedWindDegrees,
                                                                                                                           Strings.precipitationTitle,
                                                                                                                           view.model.displayedProbOfPrep,
                                                                                                                           Strings.cloudsTitle,
                                                                                                                           view.model.displayedClouds]))
                }
            }

            context("Model") {
                it("should give informations properly") {
                    let formatter: DateFormatter = .default
                    formatter.timeStyle = .short

                    expect(model.displayedTime).to(equal(model.dateTime.string(from: formatter)))
                    expect(model.displayedTemperature).to(equal(Int(model.temperature).format(with: .temperature)))
                    expect(model.displayedWindSpeed).to(equal(model.wind.speed.format(with: .speed)))
                    expect(model.displayedWindDegrees).to(equal(model.wind.degrees.format(with: .orientation)))
                    expect(model.displayedProbOfPrep).to(equal(model.precipitation.format(with: .percent)))
                    expect(model.displayedClouds).to(equal(model.clouds.format(with: .percent)))
                }
            }
        }
    }
}
