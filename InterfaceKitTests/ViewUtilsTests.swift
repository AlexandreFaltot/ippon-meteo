//
//  ViewUtilsTests.swift
//  InterfaceKitTests
//
//  Created by Alexandre FALTOT on 03/05/2021.
//

import Quick
import Nimble
import SwiftUI
@testable import InterfaceKit

class ViewUtilsTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("View utils") {
            var view: IWWeatherForecastPreviewView!

            context("CGSize") {
                it("should give a square when asked to") {
                    let square: CGSize = .square(of: 24)
                    expect(square.width).to(equal(24))
                    expect(square.height).to(equal(24))
                }
            }

            context("EgdeInsets") {
                it("should set every inset properly when asked to") {
                    let insets: EdgeInsets = .all(80)
                    expect(insets.top).to(equal(80))
                    expect(insets.bottom).to(equal(80))
                    expect(insets.leading).to(equal(80))
                    expect(insets.trailing).to(equal(80))
                }
            }
        }
    }
}

