//
//  IWViewControllerProviderTests.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 03/05/2021.
//

import Quick
import Nimble
import SwiftUI
@testable import IpponWeather

class IWViewControllerProviderTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("IWViewControllersProvider") {

            context("Providing controllers") {
                it("should provide the right wone when needed") {
                    expect(IWViewControllersProvider.home()).to(beAKindOf(IWMapView.self))
                    expect(IWViewControllersProvider.stationDetailsViewController(selectedItem: nil)).to(beAKindOf(IWStationDetailView.self))
                }
            }
        }
    }
}
