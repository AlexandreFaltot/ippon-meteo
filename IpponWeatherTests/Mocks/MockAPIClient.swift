//
//  MockAPIClient.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Foundation
import Combine
@testable import NetworkKit

public class MockAPIClient<T: Target>: ApiClient<T> {
    @Published var publisher: Decodable?

    override public func request<Response: Decodable>(_ expectedType: Response.Type, target: T) -> AnyPublisher<Response, Error> {
        do {
            guard let mockedResponseURL = Bundle(for: type(of: self)).url(forResource: String(describing: Response.self), withExtension: "json") else {
                throw MockURLError()
            }

            let mockedData = try Data(contentsOf: mockedResponseURL)
            let mockedResponse = try JSONDecoder().decode(Response.self, from: mockedData)

            return Just(mockedResponse)
                .setFailureType(to: Error.self)
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
        } catch {
            return Fail<Response, Error>(error: error)
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
        }
    }

    public override init() {}
    
    struct MockURLError: Error {}
}

class MockedTarget: Target {
    var endpoint: String = ""
}
