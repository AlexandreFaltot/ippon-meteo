//
//  MapperTests.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 03/05/2021.
//

import Quick
import Nimble
import SwiftUI
import NetworkKit
import InterfaceKit
@testable import IpponWeather

class MapperTests: QuickSpec {
    override func spec() {
        super.spec()

        describe("Mapping objects") {

            context("From Network to Interface") {
                it("should map object nicely") {
                    let weatherAPIResponse = WeatherAPIGroupResponse.decode()!
                    expect(weatherAPIResponse.list.first?.iwWeatherForecastPreviewViewModel).toNot(beNil())
                    expect(weatherAPIResponse.list.compactMap { $0.iwWeatherForecastPreviewViewModel }.count).to(equal(weatherAPIResponse.list.count))

                    let weatherAPIForecastResponse = WeatherAPIForecastResponse.decode()!
                    expect(weatherAPIForecastResponse.list.first?.iwWeatherRow).toNot(beNil())
                    expect(weatherAPIForecastResponse.list.compactMap { $0.iwWeatherRow }.count).to(equal(weatherAPIForecastResponse.list.count))
                    expect(weatherAPIForecastResponse.list.first?.iwWeatherForecastPreview).toNot(beNil())
                    expect(weatherAPIForecastResponse.list.compactMap { $0.iwWeatherForecastPreview }.count).to(equal(weatherAPIForecastResponse.list.count))
                }
            }
        }
    }
}

private extension Decodable {
    static func decode() -> Self? {
        let bundle = Bundle(for: MapperTests.self)
        guard let mockedResponseURL = bundle.url(forResource: String(describing: Self.self), withExtension: "json") else {
            return nil
        }

        return try? JSONDecoder().decode(Self.self, from: Data(contentsOf: mockedResponseURL))
    }
}
