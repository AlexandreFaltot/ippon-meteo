//
//  IpponWeatherTests.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 28/04/2021.
//

import Quick
import Nimble
import Combine
import NetworkKit
@testable import IpponWeather

class WeatherServiceTests: QuickSpec {

    override func spec() {
        super.spec()
        let service = WeatherService()
        var cancellableSet = Set<AnyCancellable>()

        beforeSuite {
            service.apiClient = MockAPIClient()
            appContainer.register(WeatherService.self) { _ in service }
        }
        describe("WeatherService") {
            context("requesting") {
                it("should get the data for current forecast") {
                    var result: [WeatherAPIStation] = []
                    service.getCurrentForecast(for: [City(id: 1, name: "Name")])
                        .sink(receiveCompletion: { _ in },
                              receiveValue: { result = $0 })
                        .store(in: &cancellableSet)
                    
                    expect(result).toEventuallyNot(beEmpty())
                }

                it("should get the data properly for station forecast") {
                    var result: WeatherAPIForecastResponse?
                    service.getFutureDaysForecast(for: 1)
                        .sink(receiveCompletion: { _ in },
                              receiveValue: { result = $0 })
                        .store(in: &cancellableSet)

                    expect(result).toEventuallyNot(beNil())
                }
            }
        }
    }
}
