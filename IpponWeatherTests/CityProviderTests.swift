//
//  CityProviderTests.swift
//  IpponWeatherTestings
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
@testable import IpponWeather

class CityProviderTests: QuickSpec {

    override func spec() {
        super.spec()

        describe("CityProvider") {
            context("providing cities") {
                it("should get the cities from json properly") {
                    expect(CityProvider.cities).toNot(beEmpty())
                }
            }
        }
    }
}
