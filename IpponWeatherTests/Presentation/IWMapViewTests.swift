//
//  IWMapViewTests.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 03/05/2021.
//

import Quick
import Nimble
import SwiftUI
import MapKit
import ViewInspector
import InterfaceKit
@testable import IpponWeather

class IWMapViewTests: QuickSpec {
    override func spec() {
        super.spec()

        var mockedService: WeatherService {
            let service = WeatherService()
            service.apiClient = MockAPIClient()
            return service
        }

        describe("IWMapViewModel") {
            let viewModel = IWMapViewModel(service: mockedService)

            context("Initializing") {
                it("should fetch annotations properly") {
                    expect(viewModel.annotations).to(beEmpty())
                    viewModel.fetchMajorCitiesWeather()
                    expect(viewModel.annotations).toEventuallyNot(beEmpty())
                }
            }
        }
    }
}
