//
//  IWStationDetailViewTests.swift
//  IpponWeatherTests
//
//  Created by Alexandre FALTOT on 03/05/2021.
//

import Quick
import Nimble
import SwiftUI
import ViewInspector
import NetworkKit
import InterfaceKit
import XCTest
@testable import IpponWeather

class IWStationDetailViewTests: QuickSpec {
    override func spec() {
        super.spec()

        var mockedService: WeatherService {
            let service = WeatherService()
            service.apiClient = MockAPIClient()
            return service
        }

        
        try! IWStationDetailView(viewModel: IWStationDetailViewModel(service: mockedService)).inspect()

        describe("IWMapViewModel") {
            var viewModel: IWStationDetailViewModel!
            beforeEach {
                viewModel = IWStationDetailViewModel(service: mockedService)
            }

            context("Initializing") {
                it("should not fetch annotations without city") {
                    viewModel.cityToFetch = nil
                    viewModel.fetchWeatherData()
                    expect(viewModel.state.isLoading).toEventually(beFalse())
                    expect(viewModel.state.isError).to(beTrue())
                    expect(viewModel.displayedList).to(beEmpty())
                    expect(viewModel.forecastsPreviews).to(beEmpty())
                }

                it("should fetch annotations with a city") {
                    viewModel.cityToFetch = City(id: 1, name: "City")
                    viewModel.fetchWeatherData()
                    expect(viewModel.state.isLoading).toEventually(beFalse())
                    expect(viewModel.state.isError).to(beFalse())
                    expect(viewModel.displayedList).toNot(beEmpty())
                    expect(viewModel.forecastsPreviews).toNot(beEmpty())
                }
            }

            context("Interracting") {
                beforeEach {
                    viewModel.cityToFetch = City(id: 1, name: "City")
                    viewModel.fetchWeatherData()
                    expect(viewModel.state.isLoading).toEventually(beFalse())
                }

                it("Should transform the data properly") {
                    viewModel.focusOn(viewModel.forecastsPreviews[1].date)
                    expect(viewModel.displayedList.count).to(equal(8))
                    viewModel.focusOn(viewModel.forecastsPreviews.first!.date)
                    expect(viewModel.displayedList.count).to(equal(2))
                    viewModel.focusOn(viewModel.forecastsPreviews.last!.date)
                    expect(viewModel.displayedList.count).to(equal(8))
                }
            }
        }
    }
}

extension IWViewState {
    var isLoading: Bool {
        switch self {
        case .loading: return true
        default: return false
        }
    }

    var isError: Bool {
        switch self {
        case .error: return true
        default: return false
        }
    }
}
