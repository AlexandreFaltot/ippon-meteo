//
//  MockedConfiguration.swift
//  NetworkKitTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import NetworkKit

class MockedConfiguration: OpenWeatherConfigurationProtocol {
    var openWeatherApiKey: String = "API_KEY"
    var openWeatherUrl: URL = URL(string: "https://api.weather.org")!
}
