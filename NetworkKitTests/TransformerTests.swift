//
//  TransformerTests.swift
//  NetworkKitTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
@testable import NetworkKit

class TransformerTests: QuickSpec {

    override func spec() {
        super.spec()

        describe("JSONDateTransformer") {
            let transformer = JSONDateTransformer()

            context("Decoding") {
                it("should decode date from string as wanted") {
                    expect(try? transformer.transform("2020-01-01 12:00:00")).toNot(beNil())
                    expect(try? transformer.transform("Hello world!")).to(beNil())
                    expect(try? transformer.transform("2020-01-01")).to(beNil())
                }
            }
        }
    }
}

