//
//  DecodableTests.swift
//  NetworkKitTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
@testable import NetworkKit

class DecodableTests: QuickSpec {

    override func spec() {
        super.spec()

        describe("API Response Data") {
            context("Decoding data from api response") {
                it("should decode WeatherAPIGroupResponse Properly") {
                    expect(WeatherAPIGroupResponse.decode()).toEventuallyNot(beNil())
                }

                it("should decode WeatherAPIForecastResponse Properly") {
                    expect(WeatherAPIForecastResponse.decode()).toEventuallyNot(beNil())
                }
            }
        }
    }
}

private extension Decodable {
    static func decode() -> Self? {
        let bundle = Bundle(for: DecodableTests.self)
        guard let mockedResponseURL = bundle.url(forResource: String(describing: Self.self), withExtension: "json") else {
            return nil
        }

        return try? JSONDecoder().decode(Self.self, from: Data(contentsOf: mockedResponseURL))
    }
}
