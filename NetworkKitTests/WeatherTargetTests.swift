//
//  ForecastTarget.swift
//  NetworkKitTests
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import Quick
import Nimble
@testable import NetworkKit

class WeatherTargetTests: QuickSpec {

    override func spec() {
        super.spec()

        var target: ForecastTarget!
        describe("ForecastTarget") {
            beforeEach {
                networkContainer.register(OpenWeatherConfigurationProtocol.self) { _ in MockedConfiguration() }
            }

            context("Cities target") {
                beforeEach {
                    target = .cities(ids: [1, 10, 20])
                }
                it("should give the right informations") {
                    expect(target.asURLRequest()).toNot(beNil())
                    expect(target.queryParameters).to(equal(["id": "1,10,20",
                                                             "units": "metric"]))
                    expect(target.endpoint).to(equal("/data/2.5/group"))
                    expect(target.httpBody).to(beNil())
                }
            }

            context("Future Days forecast target") {
                beforeEach {
                    target = .futureDaysForecast(id: 1)
                }

                it("should give the right informations") {
                    expect(target.asURLRequest()).toNot(beNil())
                    expect(target.queryParameters).to(equal(["id": "1",
                                                             "units": "metric"]))
                    expect(target.endpoint).to(equal("/data/2.5/forecast"))
                    expect(target.httpBody).to(beNil())
                }
            }
        }
    }
}

