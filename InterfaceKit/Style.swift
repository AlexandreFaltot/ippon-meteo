//
//  Style.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import SwiftUI

public struct Style {
    public struct TextFont {
        /// Font system of size 14
        public static let body: Font = Font.system(size: 14)
        /// Font system of size 18, bold
        public static let title: Font = Font.system(size: 18, weight: .bold)
    }

    public struct Padding {
        /// Insets where every edge values 8
        public static let standard: EdgeInsets = .all(8)
    }

    public struct Spacing {
        /// 16
        public static let standard: CGFloat = 16
    }

    public struct Size {
        public struct Image {
            /// Square of size 24
            public static let medium: CGSize = .square(of: 24)
            /// Square of size 32
            public static let large: CGSize = .square(of: 32)
        }
    }
}
