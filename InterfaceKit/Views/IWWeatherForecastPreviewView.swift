//
//  IWWeatherForecastPreviewView.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import SwiftUI
import MapKit

public struct IWWeatherForecastPreviewView: IWView {
    public typealias Model = IWWeatherForecastPreviewViewModel
    public var model: Model

    public init(model: Model) {
        self.model = model
    }

    public var body: some View {
        VStack(spacing: .zero) {
            model.title.map {
                Text($0)
                    .font(Style.TextFont.body)
            }
            model.image?
                .resizable()
                .scaledToFit()
                .frame(Style.Size.Image.medium)
            Text(model.displayedTemperature)
                .font(Style.TextFont.body)
        }
    }
}

public struct IWWeatherForecastPreviewViewModel {
    var title: String?
    var image: Image?
    var temperature: Double

    public init(title: String? = nil, image: Image?, temperature: Double) {
        self.title = title
        self.image = image
        self.temperature = temperature
    }

    /// The temperature string that will be displayed
    var displayedTemperature: String {
        Int(temperature).format(with: .temperature)
    }
}
