//
//  IWView.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 29/04/2021.
//

import SwiftUI

public protocol IWView: View {
    associatedtype Model
    var model: Model { get set }

    init(model: Model)
}

public protocol IWViewController: View {
    associatedtype ViewModel: IWViewModel
    var viewModel: ViewModel { get set }
}

open class IWViewModel: ObservableObject {
    public init() {}
}
