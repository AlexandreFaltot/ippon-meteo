//
//  ViewState.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 01/05/2021.
//

import Foundation

public enum IWViewState<T> {
    case loading
    case success(value: T)
    case error(error: Error)
}
