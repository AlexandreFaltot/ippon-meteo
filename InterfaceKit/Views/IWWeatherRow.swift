//
//  IWWeatherRow.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 01/05/2021.
//

import SwiftUI
import CommonKit

public struct IWWeatherRow: IWView {
    public typealias Model = IWWeatherRowModel
    public var model: IWWeatherRowModel

    public init(model: Model) {
        self.model = model
    }

    public var body: some View {
        VStack(alignment: .leading) {
            Text(model.displayedTime)
            HStack(alignment: .top,
                   spacing: Style.Spacing.standard) {
                VStack {
                    model.weatherIcon?
                        .resizable()
                        .scaledToFit()
                        .frame(Style.Size.Image.large)
                    Text(model.displayedTemperature)
                        .font(Style.TextFont.body)
                }
                .frame(maxWidth: .infinity)
                VStack {
                    Text(Strings.windTitle)
                    Text(model.displayedWindSpeed)
                        .font(Style.TextFont.body)
                    Text(model.displayedWindDegrees)
                        .font(Style.TextFont.body)
                }
                .frame(maxWidth: .infinity)
                VStack {
                    Text(Strings.precipitationTitle)
                    Text(model.displayedProbOfPrep)
                        .font(Style.TextFont.body)
                }
                .frame(maxWidth: .infinity)
                VStack {
                    Text(Strings.cloudsTitle)
                    Text(model.displayedClouds)
                        .font(Style.TextFont.body)
                }
                .frame(maxWidth: .infinity)
            }
        }
    }
}

public struct IWWeatherRowModel {
    var dateTime: Date
    var weatherIcon: Image?
    var temperature: Double
    var wind: (speed: Double, degrees: Double)
    var precipitation: Double
    var clouds: Int

    public init(dateTime: Date, weatherIcon: Image? = nil, temperature: Double, wind: (speed: Double, degrees: Double), precipitation: Double, clouds: Int) {
        self.dateTime = dateTime
        self.weatherIcon = weatherIcon
        self.temperature = temperature
        self.wind = wind
        self.precipitation = precipitation
        self.clouds = clouds
    }

    var displayedTime: String {
        let dateFormatter: DateFormatter = .default
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: dateTime)
    }

    /// The temperature string that will be displayed
    var displayedTemperature: String {
        Int(temperature).format(with: .temperature)
    }

    /// The wind speed string that will be displayed
    var displayedWindSpeed: String {
        wind.speed.format(with: .speed)
    }

    /// The wind degrees string that will be displayed
    var displayedWindDegrees: String {
        wind.degrees.format(with: .orientation)
    }

    /// The probability of precipitation string that will be displayed
    var displayedProbOfPrep: String {
        precipitation.format(with: .percent)
    }

    /// The clouds string that will be displayed
    var displayedClouds: String {
        clouds.format(with: .percent)
    }
}
