//
//  View+utils.swift
//  IpponWeather
//
//  Created by Alexandre FALTOT on 02/05/2021.
//

import SwiftUI

extension View {
    ///
    /// Method to specify a fixed size for a view’s width and height,
    /// basing on the provided size
    ///
    /// - parameter size: The size for the frame
    ///
    func frame(_ size: CGSize) -> some View {
        return frame(width: size.width, height: size.height)
    }
}


extension EdgeInsets {
    ///
    /// Method to create an `EdgeInsets` with all edge of the same length
    ///
    /// - parameter length: The length that will be used to create the object
    ///
    static func all(_ length: CGFloat) -> EdgeInsets {
        EdgeInsets(top: length, leading: length, bottom: length, trailing: length)
    }
}

extension CGSize {
    ///
    /// Method to create a `CGSize` with equal width and length
    ///
    /// - parameter length: The length that will be used to create the object
    ///
    static func square(of length: CGFloat) -> CGSize {
        CGSize(width: length, height: length)
    }
}
