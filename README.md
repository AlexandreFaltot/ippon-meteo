# Ippon Weather

## Description

### Fonctionnel

Ippon Weather permet au lancement d'afficher la météo courante sur une vingtaine de villes de France, affichées sur une map.
Au clic sur une ville, une page détaillant la météo sur les 5 jours à venir est présentée. Sur cette page, on peut naviguer entre les jours
afin d'avoir en détail la météo de chaque jour.

### Technique

L'application est basé sur une architecture MVVM et est composée de 4 modules:

- CommonKit: Le module commun aux autres, pour centraliser du code
- NetworkKit: Le module gérant les appel réseaux
- InterfaceKit: Le module contenant les éléments graphiques
- IpponWeather: Le module liant les 4 autres modules et s'occupant des règles métiers

Il y a deux targets : IpponWeather et IpponWeatherDev afin d'afficher ou non les log, et avoir un nom différent pour l'application.
Chaque module possède sa propre target de test afin d'assurer au mieux leur découpage et leur indépendance

J'ai utilisé la librairie [Swinject](https://github.com/Swinject/Swinject) pour l'injection de dépendance, ainsi que les librairies [Quick](https://github.com/Quick/Quick), [Nimble](https://github.com/Quick/Nimble), et [ViewInspector](https://github.com/nalexn/ViewInspector) pour les tests.

Les icônes utilisées ont été crées par Sihan-Liu, et trouvable [ici](https://www.iconfinder.com/iconsets/weather-color-2)

## Installation et lancement

La version minimum d'iOS utilisée pour ce projet est la 14.0
Pour lancer le projet, il faut installer les pods via cocoapods avec la commande `pod install`
